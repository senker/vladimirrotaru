[[_TOC_]]

## :open_file_folder: UCL University College Semester projects
### :books: ITT1 Greenhouse project
Greenhouse temperature and humidity measurement device developed with Python, RaspberryPi, Thingspeak cloud and Gitlab in the first semester during the Project course at UCL University College in Odense, Denmark.
<br>
Project overview: [greenhouse_folder]
<br>
Original gitlab project: [greenhouse_project]
<br>
Gitlab pages for project: [greenhouse_pages]
<br>

### :books: ITT2 Datacenter project
Datacenter temperature and humidity measurement system developed with Python, MQTT, Node-RED dashboard UI, GitLab, RaspberryPi using Scrum as the development framework. This project was done in the second semester during the Project course at UCL University College in Odense, Denmark in collaboration with students from Maskinmesterskole, Fredericia, Denmark.                                              
<br> 
Project overview: [datacenter_folder]
<br>
Original gitlab project: [datacenter_project]
<br>
Gitlab pages for project: [datacenter_pages]
<br>

### :books: ITT3 Solar panel project
Automatized dynamic solar panel developed with Python, C++, RaspberryPi, Arduino Nano, Gitlab, Actuators, CLI/GUI development using Unified Process as the development framework. This project is under progress in the third semester during the Project course at UCL University College in Odense, Denmark.
<br>
Project overview: [solar_panel_folder]
<br>
Original gitlab project: [solar_panel_project]
<br>


## About me

- 👋 Hi there, I’m Vladimir Rotaru, currently studying IT-Technology at UCL University College in Odense, Denmark

- 👀 I’m interested in software development whether it is front end or back end, applying development frameworks such as Scrum, UP etc.

- 🌱 I’m currently learning React.js and Angular on my own

- :thought_balloon: Looking forward to expand my knowledge towards machine learning 

- :satellite: I’m looking to collaborate on working together for different projects which would expand my range of knowledge

- 📫 How to reach me ... simply click on one of the icons below :small_red_triangle_down:

## Contact information
<p align="center">
  <a href="mailto:vla.rotaru@gmail.com?subject=Hello%20Vladimir"><img src="https://img.shields.io/badge/gmail-%23D14836.svg?&style=for-the-badge&logo=gmail&logoColor=white" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://www.linkedin.com/in/vladimir-rotaru/"><img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" /></a>&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>

[greenhouse_folder]: https://gitlab.com/senker/vladimirrotaru/-/tree/main/semester_projects/ITT1_greenhouse_project
[greenhouse_project]: https://gitlab.com/20a-itt1-project/team_b1
[greenhouse_pages]: https://20a-itt1-project.gitlab.io/team_b1/
[datacenter_folder]: https://gitlab.com/senker/vladimirrotaru/-/tree/main/semester_projects/ITT2_datacenter_project
[datacenter_pages]: https://21s-itt2-datacenter-students-group.gitlab.io/team-b1/
[datacenter_project]: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1
[solar_panel_folder]: https://gitlab.com/senker/vladimirrotaru/-/tree/main/semester_projects/ITT3_solar_panel_project
[solar_panel_project]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5
