#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#define BNO055_SAMPLERATE_DELAY_MS (200)

void serial_flush(void);
bool angel_check(void);
 
Adafruit_BNO055 myIMU = Adafruit_BNO055();
String received_data= "";
bool is_moving = true;


// defining the max angels the solar panel can turn
const int MaxPosX = 45.0;
const int MaxPosY = 45.0;
const int MaxPosZ = 45.0;
const int MaxNegX = -45.0;
const int MaxNegY = -45.0;
const int MaxNegZ = -45.0;
float XValue;
float YValue;
float ZValue;
bool block_m1 = false;
bool block_m2 = false;
bool block_m3 = false;
bool block_m4 = false;


// D7
const int pwm0 = 7; 
// D6
const int in_1 = 6; 
// D5
const int in_2 = 5; 

// D8
const int in_4 = 8;
// D9
const int in_3 = 9;
// D10
const int pwm1 = 10;

void setup() {
  // serial setup
    Serial.begin(115200);


  // pin setup
    pinMode(pwm0,OUTPUT); 
    pinMode(in_1,OUTPUT); 
    pinMode(in_2,OUTPUT);
    pinMode(pwm1,OUTPUT); 
    pinMode(in_3,OUTPUT); 
    pinMode(in_4,OUTPUT);

    
  // BNO055 setup
    myIMU.begin();
    int8_t temp=myIMU.getTemp();
    myIMU.setExtCrystalUse(true);

} 

void loop(){
  angel_check();
  move_actuator();
  read_data();

}


String read_data(){
     received_data = "";
     if (Serial.available() > 0) {
     received_data = Serial.readString();
     Serial.println("instructions received");
     serial_flush();
     return received_data;
     }
}

bool move_actuator() {
    analogWrite(pwm0,255);
    analogWrite(pwm1,255);
    if (received_data == "m1"){
          if (block_m1 == true){
            Serial.print("m1 blocked");
          }
          else{
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,LOW);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH); 
          is_moving = false;
          }
        }

    else if (received_data == "m2"){
      if (block_m2 == true){
            Serial.print("m2 blocked");
          }
          else{
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,LOW);
          is_moving = false;
          }
      }

   else if (received_data == "m3"){
    if (block_m3 == true){
            Serial.print("m3 blocked");
          }
          else{
          digitalWrite(in_1,LOW); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH);  
          is_moving = false;
          }
      }

   else if (received_data == "m4"){
    if (block_m4 == true){
            Serial.print("m4 blocked");
          }
          else{
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,LOW); 
          digitalWrite(in_4,HIGH);
          is_moving = false;
          }
      }
      
   else {
        digitalWrite(in_1,HIGH); 
        digitalWrite(in_2,HIGH);
        digitalWrite(in_3,HIGH); 
        digitalWrite(in_4,HIGH);
        is_moving = true;  
        //Serial.println("Waiting for instructions");
        

   }
   delay(1000);
   return is_moving; 
}

void serial_flush(void) {
  while (Serial.available()) Serial.read();
}

bool angel_check(){
  imu::Vector<3> mag =myIMU.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
  XValue = mag[0];
  YValue = mag[1];
  ZValue = mag[2];
  block_m1 = false;
  block_m2 = false;
  block_m3 = false;
  block_m4 = false;
  
  if (XValue > MaxPosX){
    block_m1 = true;
    return block_m1;
  }
  else if (XValue < MaxNegY){
    block_m2 = true;
    return block_m2;
  }
  else if (YValue > MaxPosY){
    block_m3 = true;
    return block_m3;
  }
  else if (YValue < MaxNegY){
    block_m4 = true;
    return block_m4;
  }
  else{

 }
  serial_flush();
}


